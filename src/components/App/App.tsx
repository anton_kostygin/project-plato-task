import {Provider} from 'react-redux';
import Layout from 'components/common/Layout';
import {store} from 'store';
import Header from './Header';
import Content from './Content';

export default function App() {
    return (
        <Provider store={store}>
            <Layout>
                <Header/>
                <Content/>
            </Layout>
        </Provider>
    );
}
