import List from 'components/common/List';
import {useActiveUserTodos} from 'hooks/useActiveUserTodos';
import TaskListItem from './TaskListItem';
import styles from './TaskList.module.css';

export default function TaskList() {
    const {todos, completeTodo} = useActiveUserTodos();

    return <>
        <h2>Task List</h2>
        <List className={styles.list}>
            {todos.map((todo) => (
                <TaskListItem key={todo.id} todo={todo} onComplete={completeTodo}/>))}
        </List>
    </>;
}