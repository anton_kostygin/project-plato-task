import React, {useCallback} from 'react';
import {TaskListItemPropsType} from './TaskListItem.types';
import {ListItem} from 'components/common/List';
import Task from 'components/common/Task';

function TaskListItem({todo: {id, title, completed}, onComplete}: TaskListItemPropsType) {
    const onCompleteTodo = useCallback(() => {
        onComplete(id);
    }, [onComplete, id]);

    return <ListItem>
        {
            completed ? <Task title={title} isCompleted/> : <Task onComplete={onCompleteTodo} title={title}/>
        }
    </ListItem>;

}

export default React.memo(TaskListItem);
