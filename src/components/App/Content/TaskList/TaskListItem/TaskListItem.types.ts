import {TodoItemType} from 'store/slices/todos';

export type TaskListItemPropsType = {
    todo: TodoItemType;
    onComplete: (id: number) => void;
}