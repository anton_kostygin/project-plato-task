import {useInitApp} from 'hooks/useInitApp';
import UserList from './UserList';
import TaskList from './TaskList';
import styles from './Content.module.css';

export default function Content() {
    const {isLoading} = useInitApp();

    return <div className={styles.content}>
        {
            isLoading ?
                <p>Loading...</p>
                :
                <>
                    <aside>
                        <UserList/>
                    </aside>
                    <main>
                        <TaskList/>
                    </main>
                </>
        }
    </div>;
}