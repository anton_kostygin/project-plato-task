import {useUsers} from 'hooks/useUsers';
import {useActiveUser} from 'hooks/useActiveUser';
import List from 'components/common/List';
import UserListItem from './UserListItem';
import styles from './UserList.module.css';


export default function UserList() {
    const {id: activeUserId, setActiveUser} = useActiveUser();
    const {users} = useUsers();

    return <>
        <h2>Users</h2>
        <List className={styles.list}>
            {users.map((user) => (
                <UserListItem
                    key={user.id}
                    user={user}
                    isActive={user.id === activeUserId}
                    setIsActive={setActiveUser}
                />))}
        </List>
    </>;
}