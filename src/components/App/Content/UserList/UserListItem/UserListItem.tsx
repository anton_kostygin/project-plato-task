import React, {useCallback} from 'react';
import {UserListItemPropsType} from './UserListItem.types';
import {ListItem} from 'components/common/List';
import User from 'components/common/User';

function UserListItem({user, isActive, setIsActive}: UserListItemPropsType) {
    const onUserClick = useCallback(() => {
        setIsActive(user.id);
    }, [setIsActive, user.id]);

    return <ListItem>
        <User onClick={onUserClick} isActive={isActive} name={user.name}/>
    </ListItem>;
}

export default React.memo(UserListItem);
