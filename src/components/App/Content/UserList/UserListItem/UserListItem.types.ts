import {UserType} from 'store/slices/users';

export type UserListItemPropsType = {
    user: UserType;
    isActive: boolean;
    setIsActive: (id: number) => void;
}