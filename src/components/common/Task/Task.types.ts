export type TaskPropsType<Completed extends boolean> =  (Completed extends false ? {
    title: string,
    onComplete?: () => void
    isCompleted?: Completed;
} : {
    title: string,
    isCompleted: Completed;
    onComplete?: never;
})