import {ReactComponent as CheckIcon} from 'assets/check.svg';
import {TaskPropsType} from './Task.types';
import styles from './Task.module.css';

export default function Task<TCompleted extends boolean>({title, isCompleted, onComplete}: TaskPropsType<TCompleted>) {
    if (!isCompleted) {
        return <label className={styles.activeTask}><input type={'checkbox'} onChange={onComplete}/><span>{title}</span></label>;
    }

    return <p className={styles.completedTask}><CheckIcon /><span>{title}</span></p>
}