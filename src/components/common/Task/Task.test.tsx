import {render, fireEvent} from '@testing-library/react';
import Task from './Task';
import styles from './Task.module.css';

const TASK_NAME = '__TEST_TASK_NAME__';

describe('Task', () => {
    it('render', () => {
        const {getByText} = render(<Task title={TASK_NAME}/>);
        expect(getByText(TASK_NAME)).toBeInTheDocument();
    });

    describe('Task: isCompleted=false', () => {
        it('render', () => {
            const {getByLabelText, container: {firstChild}} = render(<Task title={TASK_NAME} isCompleted={false}/>);

            expect(getByLabelText(TASK_NAME)).toBeInTheDocument();
            expect(firstChild).toHaveClass(styles.activeTask);
        });

        it('onComplete', () => {
            const mockOnComplete = jest.fn();
            const {getByLabelText} = render(<Task title={TASK_NAME} isCompleted={false} onComplete={mockOnComplete}/>);

            fireEvent.click(getByLabelText(TASK_NAME));
            expect(mockOnComplete).toBeCalledTimes(1);
        });
    });

    describe('Task: isCompleted=true', () => {
        it('render', () => {
            const {queryByLabelText, getByText, container: {firstChild}} = render(<Task title={TASK_NAME} isCompleted/>);

            expect(queryByLabelText(TASK_NAME)).not.toBeInTheDocument();
            expect(getByText(TASK_NAME)).toBeInTheDocument();
            expect(firstChild).toHaveClass(styles.completedTask);
        });

        it('onComplete', () => {
            const mockOnComplete = jest.fn();
            const {getByText} = render(
                <Task
                    title={TASK_NAME}
                    isCompleted
                    // @ts-ignore
                    onComplete={mockOnComplete}
                />
            );

            fireEvent.click(getByText(TASK_NAME));
            expect(mockOnComplete).not.toBeCalled();
        });
    })
});