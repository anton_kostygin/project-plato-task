import {ReactComponent as ArrowIcon} from 'assets/arrow-right.svg';
import {UserPropsType} from './User.types';
import styles from './User.module.css';

export default function User({name, isActive, onClick}: UserPropsType) {
    return <button className={styles.user} onClick={onClick}>
        {name}
        {isActive && <ArrowIcon/>}
    </button>;
}