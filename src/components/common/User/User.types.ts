export type UserPropsType =  {
    name: string,
    isActive: boolean
    onClick: () => void;
}