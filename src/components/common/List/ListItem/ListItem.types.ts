import {ReactNode} from 'react';

export type ListItemPropsType = {
    className?: string
    children: ReactNode;
    onClick?: () => void
}