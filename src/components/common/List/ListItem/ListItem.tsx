import {ListItemPropsType} from './ListItem.types';
import classNames from 'classnames';
import styles from './ListItem.module.css';

export default function ListItem({children, className, onClick}: ListItemPropsType) {
    return <li className={classNames(styles.listItem, className)} onClick={onClick}>
        {children}
    </li>;
}