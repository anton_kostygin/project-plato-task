import classNames from 'classnames';
import {ListPropsType} from './List.types';
import styles from './List.module.css';

export default function List({children, className }: ListPropsType) {
    return <ul className={classNames(styles.list, className)}>
        {children}
    </ul>;
}