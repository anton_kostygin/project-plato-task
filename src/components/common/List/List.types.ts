import {ReactNode} from 'react';

export type ListPropsType = {
    children: ReactNode;
    className?: string;
}