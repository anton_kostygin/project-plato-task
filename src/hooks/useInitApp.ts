import {useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {TodoResponseType, UserResponseType} from 'types/api';
import {setUsers} from '../store/slices/users';
import {DispatchType} from '../store';
import {buildStoredUsers} from '../utils/user';
import {setTodos} from '../store/slices/todos';
import {buildStoredTodos} from '../utils/todo';
import {setActiveUser} from '../store/slices/activeUser';


export function useInitApp() {
    const dispatch = useDispatch<DispatchType>();
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
            const usersReq = await fetch('https://jsonplaceholder.typicode.com/users');
            const todosReq = await fetch('https://jsonplaceholder.typicode.com/todos');

            const [usersData, todosData]: [UserResponseType[], TodoResponseType[]] = await Promise.all([usersReq.json(), todosReq.json()]);

            dispatch(setUsers(buildStoredUsers(usersData)));
            dispatch(setTodos(buildStoredTodos(todosData)));
            dispatch(setActiveUser(usersData[0]?.id))
            setIsLoading(false);
        };

        fetchData();
    }, [dispatch]);

    return {isLoading};
}