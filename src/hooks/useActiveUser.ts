import {useDispatch, useSelector} from 'react-redux';
import {DispatchType, StoreType} from '../store';
import {useCallback} from 'react';
import {setActiveUser as setActiveUserAction} from '../store/slices/activeUser';

export function useActiveUser() {
    const dispatch = useDispatch<DispatchType>();
    const id = useSelector<StoreType, number>((store) => store.activeUser.id as number);

    const setActiveUser = useCallback((id: number) => {
        dispatch(setActiveUserAction(id))
    }, [dispatch])

    return {id, setActiveUser};
}