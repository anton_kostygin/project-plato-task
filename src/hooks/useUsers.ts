import {useSelector} from 'react-redux';
import {StoreType} from '../store';
import {UserStoreType} from '../store/slices/users';

export function useUsers() {
  const users =  useSelector<StoreType, UserStoreType>((rootState) => rootState.users);
  return {users}
}