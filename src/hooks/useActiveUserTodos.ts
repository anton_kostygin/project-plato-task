import {useDispatch, useSelector} from 'react-redux';
import {DispatchType, StoreType} from '../store';
import {completeTodo as completeTodoAction, TodoItemType} from '../store/slices/todos';
import {useActiveUser} from './useActiveUser';
import {useCallback} from 'react';

export function useActiveUserTodos() {
    const {id: activeUserId} = useActiveUser();
    const dispatch = useDispatch<DispatchType>();
    const todos = useSelector<StoreType, TodoItemType[]>((store) => store.todos[activeUserId] || []);

    const completeTodo = useCallback((id: number) => {
        dispatch(completeTodoAction({userId: activeUserId, todoId: id}))
    }, [activeUserId, dispatch])

    return {todos, completeTodo};
}