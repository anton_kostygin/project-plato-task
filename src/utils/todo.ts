import {TodoResponseType} from '../types/api';
import {TodoStoreType} from '../store/slices/todos';

export function buildStoredTodos(todos: TodoResponseType[]): TodoStoreType {
    return todos.reduce<TodoStoreType>((acc, {userId, ...rest}) => {
        const usersTodos = acc[userId] || [];
        acc[userId] = [...usersTodos, rest]

        return acc;
    }, {});
}