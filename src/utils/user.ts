import {UserResponseType} from '../types/api';
import {UserStoreType} from '../store/slices/users';

export function buildStoredUsers(users: UserResponseType[]): UserStoreType {
    return users.map(({id, name}) => ({id, name}));
}