export type TodoResponseType = {
    completed: boolean;
    id: number;
    title: string;
    userId: number;
}

export type UserResponseType = {
    id: number;
    name: string;
    username: string;
    phone: string;
    website: string;
    email: string;
    address: unknown;
    company: unknown;
};