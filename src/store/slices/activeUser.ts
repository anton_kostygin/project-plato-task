import {createSlice, PayloadAction} from '@reduxjs/toolkit';


export type ActiveUserStoreType = { id?: number }

const initialState: ActiveUserStoreType = {id: undefined};

export const activeUserSlice = createSlice({
    name: 'activeUser',
    initialState,
    reducers: {
        setActiveUser: (state, action: PayloadAction<number>) => {
            state.id = action.payload;
            
            return state;
        },
    },
});

export const {setActiveUser} = activeUserSlice.actions;

const activeUserReducer = activeUserSlice.reducer;

export default activeUserReducer;