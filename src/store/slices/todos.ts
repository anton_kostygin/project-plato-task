import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {TodoResponseType} from 'types/api';

export type TodoItemType = Pick<TodoResponseType, 'id' | 'completed' | 'title'>

export type TodoStoreType = Record<TodoResponseType['userId'], TodoItemType[]>

const initialState: TodoStoreType = {};

export const todosSlice = createSlice({
    name: 'todos',
    initialState,
    reducers: {
        setTodos: (state, action: PayloadAction<TodoStoreType>) => {
            return action.payload;
        },
        completeTodo: (state, {payload: {userId, todoId}}: PayloadAction<{ userId: number, todoId: number }>) => {
            const todos = state[userId].map((todo) => {
                if (todo.id === todoId) {
                    return  {...todo, completed: true}
                }

                return todo;
            });

            state[userId] = todos;

            return state;
        }
    },
});

export const {setTodos, completeTodo} = todosSlice.actions;

const todosSliceReducer = todosSlice.reducer;

export default todosSliceReducer;