import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import {UserResponseType} from '../../types/api';

export type UserType = Pick<UserResponseType, 'id' | 'name'>

export type UserStoreType = UserType[];

const initialState: UserStoreType = []

export const usersSlice = createSlice({
    name: 'users',
    initialState,
    reducers: {
        setUsers: (state, action: PayloadAction<UserStoreType>) => {
             return action.payload
        },
    },
})

export const { setUsers } = usersSlice.actions

const userSliceReducer = usersSlice.reducer;

export default userSliceReducer;