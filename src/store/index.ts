import { configureStore } from '@reduxjs/toolkit'
import userSliceReducer from './slices/users';
import todosSliceReducer from './slices/todos';
import activeUserReducer from './slices/activeUser';

export const store = configureStore({
    reducer: {
        activeUser: activeUserReducer,
        users: userSliceReducer,
        todos: todosSliceReducer
    },
})

export type StoreType = ReturnType<typeof store.getState>
export type DispatchType = typeof store.dispatch